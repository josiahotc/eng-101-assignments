# Malaria

## Hook:

Laying in bed in the middle of sub-saharen africa, thinking about the mosquitoes living on my own blood

## Thesis:

Struggle is crucial to human flourishing
	
## Compare and contrast:

My life today, working, vs. my experiences in Niger

Today, I go to work, it's interesting but there's no real risk, and I go home

In Niger, I did crazy things, was always out of my comfort zone, and on several occasions was profoundly miserable. I was unhappy, and then I got over or around that adversity. And I grew as a person much more rapidly.


## Notes:

* This does not mean that suffering in itself is good
* It is simply that suffering is an effective means of delivering the struggle that is crucial to building up the human experience
* The same is true of games - nobody wants to play a game that's not challenging
* Looking back at struggles overcome gives us the satisfaction of progressing as a person
* It forces people to find inner strength and reckon with reality
